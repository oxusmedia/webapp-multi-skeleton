-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-05-2020 a las 09:37:55
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de datos: `webapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
CREATE TABLE IF NOT EXISTS `cuentas` (
                                         `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                         `cuenta` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `dir` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `active` tinyint(1) DEFAULT 1,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`id`, `cuenta`, `dir`, `active`) VALUES
(1, 'Cuenta 1', 'cuenta1', 1),
(2, 'Cuenta 2', 'cuenta2', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
                                          `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                          `usuario` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                          `pass` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `role` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `token` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                          `ultimoLogin` datetime DEFAULT NULL,
                                          PRIMARY KEY (`id`),
                                          UNIQUE KEY `usuario` (`usuario`),
                                          KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `pass`, `nombre`, `email`, `role`, `token`, `ultimoLogin`) VALUES
(1, 'admin', 'c8837b23ff8aaa8a2dde915473ce0991', 'Administrador', 'admin@empresa.com', 'A', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioscuentas`
--

DROP TABLE IF EXISTS `usuarioscuentas`;
CREATE TABLE IF NOT EXISTS `usuarioscuentas` (
                                                 `usuario_id` bigint(20) NOT NULL,
                                                 `cuenta_id` bigint(20) NOT NULL,
                                                 `role` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
                                                 PRIMARY KEY (`usuario_id`,`cuenta_id`),
                                                 UNIQUE KEY `cuenta_id` (`cuenta_id`,`usuario_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;
