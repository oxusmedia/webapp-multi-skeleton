<?php

namespace app\models;

class CuentaRole
{
    const
        ROLE_NONE   = 'N',
        ROLE_VIEWER = 'V',
        ROLE_ADMIN  = 'A'
    ;

    static public function getDescripcion($role = null)
    {
        $arr = array(
            self::ROLE_NONE   => 'Sin acceso',
            self::ROLE_VIEWER => 'Ver reportes',
            self::ROLE_ADMIN  => 'Administrar'
        );

        if ($role == null)
            return $arr;
        elseif (isset($arr[$role]))
            return $arr[$role];

        return false;
    }

}