<?php

namespace app\models;

class AgenciaRole
{
    const
        ROLE_ADMIN = 'A',
        ROLE_USER  = 'U'
    ;

    static public function getDescripcion($role = null)
    {
        $arr = array(
            self::ROLE_ADMIN => 'Administrador',
            self::ROLE_USER  => 'Usuario'
        );

        if ($role == null)
            return $arr;
        elseif (isset($arr[$role]))
            return $arr[$role];

        return false;
    }

}