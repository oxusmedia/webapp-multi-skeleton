<?php

namespace app\models;

use oxusmedia\webAppMulti\Cuenta;
use oxusmedia\webAppMulti\webApp;

class Usuario
{
    public $id, $nombre, $email, $role;

    public function __construct($id = null)
    {
        if ($id == null)
            $id = webApp::app()->getUsuarioId();

        $usuario = webApp::app()->db()->queryRow('SELECT * FROM usuarios WHERE id = :id', array(
            'id' => $id
        ));

        if ($usuario) {

            $this->id     = $usuario->id;
            $this->nombre = $usuario->nombre;
            $this->email  = $usuario->email;
            $this->role   = $usuario->role;

            return true;
        }

        return false;

    }

    public function getCuentas()
    {
        $db = webApp::app()->db();

        if ($this->role == webApp::ROLE_ADMIN)

            $cuentas = $db->query('SELECT cuentas.* FROM cuentas WHERE active = :active ORDER BY cuenta', array(
                'usuario_id' => $this->id,
                'active'     => 1
            ));

        else

            $cuentas = $db->query('SELECT cuentas.*, usuarioscuentas.role FROM usuarioscuentas LEFT JOIN cuentas ON usuarioscuentas.cuenta_id = cuentas.id WHERE usuarioscuentas.usuario_id = :usuario_id AND active = :active ORDER BY cuenta', array(
                'usuario_id' => $this->id,
                'active'     => 1
            ));

        $ctas = array();

        while ($cuenta = $db->getRow($cuentas)) {

            $cta = new Cuenta($cuenta);

            $cta->role = $this->role == webApp::ROLE_ADMIN ? webApp::ROLE_ADMIN : $cuenta->role;

            $ctas[] = $cta;

        }

        return $ctas;

    }

}
