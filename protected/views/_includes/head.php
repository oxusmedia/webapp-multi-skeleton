
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<base href="<?php echo $this->webApp()->getSite();?>">

<link rel="shortcut icon" href="<?php echo $this->webApp()->getUrlAssets();?>images/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo $this->webApp()->getUrlAssets();?>images/favicon.ico" type="image/x-icon">

<?php $this->renderHeadIncludes();?>

<link href="<?php echo $this->getAntiCacheURL($this->webApp()->getUrlAssets() . 'css/metisMenu.min.css');?>" rel="stylesheet">
<link href="<?php echo $this->getAntiCacheURL($this->webApp()->getUrlAssets() . 'css/startmin.css');?>" rel="stylesheet">
<link href="<?php echo $this->webApp()->getUrlAssets();?>css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="<?php echo $this->getAntiCacheURL($this->webApp()->getUrlAssets() . 'js/metisMenu.min.js');?>"></script>
<script src="<?php echo $this->getAntiCacheURL($this->webApp()->getUrlAssets() . 'js/startmin.js');?>"></script>

<?php if ($this->webApp()->isLoggedIn()) {
    $this->renderBusinessInclude("head");
} ?>
