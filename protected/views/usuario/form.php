
<?php echo $form->render();?>

<script>

    $(document).ready(function(){
        $('#role').change();
    });

    function changeRol(role)
    {
        if (role == '<?php echo \app\models\AgenciaRole::ROLE_ADMIN;?>') {
            $('.nav-tabs li:last').hide();
        }else{
            $('.nav-tabs li:last').show();
        }
    }

</script>
