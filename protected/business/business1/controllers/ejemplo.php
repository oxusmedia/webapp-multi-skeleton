<?php

namespace business\controllers;

use app\models\GeneralModel;
use business\models\BusinessModel;
use oxusmedia\webAppMulti\controller;

class ejemplo extends controller
{
    public function index()
    {
        $this->webApp()->requireLoginRedir();

        $this->titulo = 'Página de negocio';

        // invocar model de carpeta protected/models
        new GeneralModel();

        // invocar model de carpeta models del business
        new BusinessModel();

        $this->render('index');
    }

}