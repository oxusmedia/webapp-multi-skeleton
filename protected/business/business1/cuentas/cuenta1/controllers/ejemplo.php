<?php

namespace cuenta\controllers;

use app\models\GeneralModel;
use business\models\BusinessModel;
use cuenta\models\CuentaModel;
use oxusmedia\webAppMulti\controller;

class ejemplo extends controller
{
    public function index()
    {
        $this->webApp()->requireLoginRedir();

        $this->titulo = 'Página de cuenta';

        // invocar model de carpeta protected/models
        new GeneralModel();

        // invocar model de carpeta models del business
        new BusinessModel();

        // invocar model de carpeta models de la account
        new CuentaModel();

        $this->render('index');
    }

}