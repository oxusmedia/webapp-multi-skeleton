<?php

use oxusmedia\webAppMulti\controller;

class cuenta extends controller
{
    public function select($params)
    {
        $this->webApp()->requireLoginRedir();

        if (isset($params['id'])) {

            $this->webApp()->setCuenta($params['id']);

            $this->redirect($_SERVER["HTTP_REFERER"]);

        }

    }

}
