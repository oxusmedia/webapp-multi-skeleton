<?php

use app\models\AgenciaRole;
use app\models\CuentaRole;

use oxusmedia\webAppMulti\webApp;
use oxusmedia\webAppMulti\controller;
use oxusmedia\webApp\grid;
use oxusmedia\webApp\form;
use oxusmedia\webApp\tabs;
use oxusmedia\webApp\tab;
use oxusmedia\webApp\column;
use oxusmedia\webApp\input;
use oxusmedia\webApp\hidden;
use oxusmedia\webApp\password;
use oxusmedia\webApp\select;
use oxusmedia\webApp\gridActionButton;
use oxusmedia\webApp\notificacion;

class usuario extends controller
{
    public function index()
    {
        $this->webApp()->requireLoginRedir(webApp::ROLE_ADMIN);

        $this->titulo = 'Usuarios';

        $grid = $this->configGrid();

        $this->render("index", array(
            'grid' => $grid
        ));
    }

    private function configGrid()
    {
        $grid = new grid('usuarios');

        $grid
            ->setJsonUrl($this->getMethodUrl('data'))
            ->setUniqueIdFields('id')
            ->setColModel(array(
                array(
                    'name'   => 'nombre',
                    'width'  => 200,
                    'format' => grid::FMT_STRING
                ),
                array(
                    'name'   => 'email',
                    'width'  => 200,
                    'format' => grid::FMT_STRING
                ),
                array(
                    'name'          => 'role',
                    'width'         => 150,
                    'format'        => grid::FMT_SELECT,
                    'formatoptions' => array('value' => AgenciaRole::getDescripcion())
                ),
                array(
                    'name'   => 'ultimoLogin',
                    'label'  => 'Última sesión',
                    'format' => grid::FMT_DATETIME
                )
            ))
            ->setDefaultSortName('nombre')
            ->setDefaultSortOrder('asc')
            ->setActions(array(
                new gridActionButton(gridActionButton::ADD, $this->webApp()->getSite() . 'usuario/add'),
                new gridActionButton(gridActionButton::EDIT, $this->webApp()->getSite() . 'usuario/edit'),
                new gridActionButton(gridActionButton::MULTI_DELETE, $this->webApp()->getSite() . 'usuario/delete')
            ));

        return $grid;
    }

    public function add()
    {
        $this->webApp()->requireLogin(webApp::ROLE_ADMIN);

        $form = new form('usuario', array(

            new tabs('tabs', array(

                new tab('datos', array(

                    new column(array(

                        new input('email', array(
                            'rules' => array(
                                'required' => true,
                                'email'    => true
                            )
                        )),

                        new password('pass', array(
                            'label' => 'Contraseña',
                            'rules' => array(
                                'required' => true
                            )
                        )),

                        new input('nombre', array(
                            'rules' => array(
                                'required' => true
                            )
                        )),

                        new select('role', AgenciaRole::getDescripcion(), array(
                            'label'       => 'Rol en agencia',
                            'htmlOptions' => array(
                                'onchange' => "changeRol($(this).val());"
                            )
                        ))

                    ))

                ), array(
                    'title' => 'Datos del usuario'
                )),

                new tab('cuentas', array(

                    new column($this->getCuentasRoleSelect($col2)),

                    new column($col2)

                ))

            ))

        ), array(
            'action' => $this->webApp()->getSite() . 'usuario/add',
            'ajax'   => true,
            'gridId' => "usuarios"
        ));

        if (isset($_POST['usuario'])) {

            $form->setAtributes($_POST['usuario']);

            if ($form->validate()) {

                $param = $form->getAtributes();

                $param['pass'] = md5($param['pass']);

                $ctasRoles = $this->getCuentasRoles($param);

                $this->db()->insert('usuarios', $param);

                $id = $this->db()->insertId();

                foreach ($ctasRoles as $cta => $role) {

                    if ($role != CuentaRole::ROLE_NONE)

                        $this->db()->insert('usuarioscuentas', array(
                            'usuario_id' => $id,
                            'cuenta_id'  => $cta,
                            'role'       => $role
                        ));

                }

                $this->returnJson(array(
                    'error' => 0
                ));

            }

        } else {

            $this->render('form', array(
                'form' => $form
            ));

        }

    }

    public function edit()
    {
        $this->webApp()->requireLogin(webApp::ROLE_ADMIN);

        $usuario = $this->db()->queryRow('SELECT id, usuario, email, nombre, role FROM usuarios WHERE id = :id', array(
            'id' => isset($_POST['usuario']['id']) ? $_POST['usuario']['id'] : $_POST['id']
        ));

        if ($usuario) {

            $htmlOptionsAgenciaRole = array(
                'onchange' => "changeRol($(this).val());"
            );

            if ($usuario->usuario == 'admin')
                $htmlOptionsAgenciaRole['disabled'] = 'disabled';

            $form = new form('usuario', array(

                new tabs('tabs', array(

                    new tab('datos', array(

                        new column(array(

                            new hidden('id'),

                            new input('email', array(
                                'rules' => array(
                                    'required' => true,
                                    'email'    => true
                                )
                            )),

                            new password('pass', array(
                                'label'       => 'Contraseña',
                                'htmlOptions' => array(
                                    'placeholder' => 'dejar vacío para no cambiar la contraseña'
                                )
                            )),

                            new input('nombre', array(
                                'rules' => array(
                                    'required' => true
                                )
                            )),

                            new select('role', AgenciaRole::getDescripcion(), array(
                                'label'       => 'Rol en agencia',
                                'htmlOptions' => $htmlOptionsAgenciaRole
                            ))

                        ))

                    ), array(
                        'title' => 'Datos del usuario'
                    )),

                    new tab('cuentas', array(

                        new column($this->getCuentasRoleSelect($col2)),

                        new column($col2)

                    ))

                ))

            ), array(
                'action' => $this->webApp()->getSite() . 'usuario/edit',
                'ajax'   => true,
                'gridId' => "usuarios"
            ));

            if (isset($_POST['usuario'])) {

                $form->setAtributes($_POST['usuario']);

                if ($form->validate()) {

                    $param = $form->getAtributes();

                    if (!empty($param['pass']))
                        $param['pass'] = md5($param['pass']);
                    else
                        unset($param['pass']);

                    $ctasRoles = $this->getCuentasRoles($param);

                    $this->db()->update('usuarios', $param,
                        array(
                            'id' => $param['id']
                        )
                    );

                    foreach ($ctasRoles as $ctaId => $role) {

                        if ($role == CuentaRole::ROLE_NONE) {

                            $this->db()->delete('usuarioscuentas',
                                array(
                                    'usuario_id' => $usuario->id,
                                    'cuenta_id'  => $ctaId
                                )
                            );

                        } else {

                            $this->db()->insert('usuarioscuentas', array(
                                'usuario_id' => $usuario->id,
                                'cuenta_id'  => $ctaId,
                                'role'       => $role
                            ), true);

                            if ($this->db()->affectedRows() == 0)

                                $this->db()->update('usuarioscuentas',
                                    array(
                                        'role' => $role
                                    ),
                                    array(
                                        'usuario_id' => $usuario->id,
                                        'cuenta_id'  => $ctaId
                                    )
                                );

                        }

                    }

                    $this->returnJson(array(
                        'error' => 0
                    ));

                }

            } else {

                $cuentas = $this->db()->query('SELECT cuentas.*, usuarioscuentas.role FROM cuentas LEFT JOIN usuarioscuentas ON usuarioscuentas.cuenta_id = cuentas.id WHERE usuarioscuentas.usuario_id = :usuario_id', array(
                    'usuario_id' => $usuario->id
                ));

                $form->setAtributes($usuario);

                $ctasRoles = array();

                while ($cta = $this->db()->getRow($cuentas))

                    $ctasRoles['cuentarole_' . $cta->id] = $cta->role;

                $form->setAtributes($ctasRoles);

                $this->render('form', array(
                    'form' => $form
                ));

            }

        }

    }

    public function delete()
    {
        $this->webApp()->requireLogin(webApp::ROLE_ADMIN);

        if (isset($_POST['id'])) {

            $db = $this->db();

            $usuario = $db->queryRow('SELECT * FROM usuarios WHERE id IN(:ids) AND usuario = :usuario', array(
                'usuario' => 'admin',
                'ids'     => implode(',', $_POST['id'])
            ));

            if (!$usuario) {

                $db->query('DELETE FROM usuarioscuentas WHERE usuario_id IN(:ids)', array(
                    'ids' => implode(',', $_POST['id'])
                ));

                $db->query('DELETE FROM usuarios WHERE id IN(:ids)', array(
                    'ids' => implode(',', $_POST['id'])
                ));

                $this->returnJson(array(
                    'error' => 0
                ));

            } else {

                $this->returnJson(array(
                    'error'   => 1,
                    'mensaje' => 'No se permite eliminar el usuario admin.'
                ));

            }

        }

    }

    public function data()
    {
        $this->webApp()->requireLogin(webApp::ROLE_ADMIN);

        $grid = $this->configGrid();

        $grid->renderData($this->db(), "SELECT * FROM usuarios");
    }

    public function miperfil()
    {
        $this->webApp()->requireLoginRedir();

        $this->titulo = 'Mi perfil';

        $form = new form('usuario', array(

            new column(array(

                new input('email', array(
                    'rules' => array(
                        'required' => true,
                        'email'    => true
                    )
                )),

                new password('pass', array(
                    'label'       => 'Contraseña',
                    'htmlOptions' => array(
                        'placeholder' => 'dejar vacío para no cambiar la contraseña'
                    )
                )),

                new input('nombre', array(
                    'rules' => array(
                        'required' => true
                    )
                )),

                new select('theme', array(
                    webApp::THEME_LIGHT  => 'Claro',
                    webApp::THEME_DARKLY => 'Oscuro'
                ), array(
                    'label' => 'Tema'
                ))

            ))

        ));

        if (isset($_POST['usuario'])) {

            $form->setAtributes($_POST['usuario']);

            if ($form->validate()) {

                $param = $form->getAtributes();

                if (!empty($param['pass']))
                    $param['pass'] = md5($param['pass']);
                else
                    unset($param['pass']);

                $this->db()->update('usuarios', $param,
                    array(
                        'id' => $this->webApp()->getUsuarioId()
                    )
                );

                $this->webApp()->setTheme($param['theme']);

                $this->notify('Sus datos se actualizaron correctamente', notificacion::SUCCESS);

            }

        } else {

            $usuario = $this->db()->queryRow('SELECT email, nombre, theme FROM usuarios WHERE id = :id', array(
                'id' => $this->webApp()->getUsuarioId()
            ));

            $form->setAtributes($usuario);

        }

        $this->render("miperfil", array(
            'form' => $form
        ));
    }

    public function theme()
    {
        $this->webApp()->requireLoginRedir();

        if (isset($_GET['id'])) {

            if ($this->webApp()->setTheme($_GET['id'])) {

                $this->db()->update('usuarios',
                    array(
                        'theme' => $_GET['id']
                    ),
                    array(
                        'id' => $this->webApp()->getUsuarioId()
                    )
                );

            }

            $this->redirect($_SERVER['HTTP_REFERER']);

        }

    }

    private function getCuentasRoleSelect(&$col2)
    {
        $cuentas = $this->db()->query('SELECT * FROM cuentas WHERE active = :active ORDER BY cuenta', array(
            'active' => 1
        ));

        $col1 = array();
        $col2 = array();
        $n    = 0;

        while ($c = $this->db()->getRow($cuentas)) {

            $n ++;

            $permiso = new select('cuentarole_' . $c->id . '', CuentaRole::getDescripcion(), array(
                'label' => $c->cuenta
            ));

            if ($n % 2 != 0)
                $col1[] = $permiso;
            else
                $col2[] = $permiso;

        }

        return $col1;

    }

    private function getCuentasRoles(&$param)
    {
        $arr = array();

        foreach ($param as $f => $v) {

            if (strpos($f, 'cuentarole_') !== false) {

                $arr[str_replace('cuentarole_', '', $f)] = $v;

                unset($param[$f]);

            }

        }

        return $arr;
    }

}