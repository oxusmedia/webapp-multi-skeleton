<?php

return array(

    'SITE'             => 'http://localhost/',
    'TITULO'           => 'SitioAdmin',

    'DEBUG'            => true,

    'LOGIN_WITH_EMAIL' => true,

    'DB_SERVER'        => 'localhost',
    'DB_USER'          => '',
    'DB_PASS'          => '',
    'DB_DB'            => '',

    'DIR_PROTECTED'    => $_SERVER['DOCUMENT_ROOT'] . '/protected/',

//    'MAINTENANCE_MODE_ENABLED'    => true,
//    'MAINTENANCE_MODE_PASS'       => '12345',
//    'MAINTENANCE_MODE_NOT_FOUND'  => true,
//    'MAINTENANCE_MODE_BACKGROUND' => '#333',
//    'MAINTENANCE_MODE_COLOR'      => '#CCC',
//    'MAINTENANCE_MODE_LOGO'       => '/assets/images/logo.jpg',

);